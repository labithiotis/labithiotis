require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });

const { NODE_ENV, CONTENTFUL_SPACE_ID, CONTENTFUL_ACCESS_TOKEN } = process.env;

module.exports = {
  siteMetadata: {
    title: `Labithiotis`,
    description: `A Software Engineer working remotely from Austrian mountains`,
    author: `@labithiotis`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-react-helmet`,
    },
    {
      resolve: `gatsby-plugin-web-font-loader`,
      options: {
        fonts: [
          {
            family: 'Rubik',
            weights: ['300', '500', '700'],
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        displayName: NODE_ENV !== 'production',
      },
    },
    {
      resolve: `gatsby-plugin-sharp`,
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /.*\.svg$/,
        },
      },
    },
    {
      resolve: `gatsby-plugin-offline`,
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Labithiotis`,
        short_name: `Labithiotis`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: CONTENTFUL_SPACE_ID,
        accessToken: CONTENTFUL_ACCESS_TOKEN,
        downloadLocal: true,
      },
    },
    {
      resolve: `gatsby-transformer-sharp`,
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-prismjs`,
          },
          // {
          //   resolve: `gatsby-remark-images`,
          //   options: {
          //     maxWidth: 800,
          //     backgroundColor: 'var(--color-background)',
          //   },
          // },
          {
            resolve: `gatsby-remark-images-contentful`,
            options: {
              maxWidth: 800,
              backgroundColor: 'var(--color-background)',
            },
          },
        ],
      },
    },
  ],
};
