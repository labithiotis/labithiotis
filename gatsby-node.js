var path = require('path');

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  return new Promise((resolve, reject) => {
    const blogPostTemplate = path.resolve('src/templates/BlogPost.tsx');
    const privacyTemplate = path.resolve('src/templates/Privacy.tsx');
    const termsTemplate = path.resolve('src/templates/Terms.tsx');
    resolve(
      graphql(`
        {
          allContentfulBlogPost {
            edges {
              node {
                id
                slug
              }
            }
          }
          allContentfulApps {
            edges {
              node {
                name
                slug
                storeApple
                storeGoogle
              }
            }
          }
        }
      `).then((result) => {
        if (result.errors) {
          reject(result.errors);
        }
        result.data.allContentfulBlogPost.edges.forEach((edge) => {
          createPage({
            path: `/blog/${edge.node.slug}`,
            component: blogPostTemplate,
            context: edge.node,
          });
        });
        result.data.allContentfulApps.edges.forEach((edge) => {
          createPage({
            path: `/apps/privacy/${edge.node.slug}`,
            component: privacyTemplate,
            context: edge.node,
          });
        });
        result.data.allContentfulApps.edges.forEach((edge) => {
          createPage({
            path: `/apps/terms/${edge.node.slug}`,
            component: termsTemplate,
            context: edge.node,
          });
        });
      })
    );
  });
};
