import Img, { GatsbyImageProps } from 'gatsby-image';
import React from 'react';

import { getImageData } from '../utils/getImageData';

const Image = ({ image, ...props }: { image: string } & GatsbyImageProps) => {
  return <Img fluid={getImageData(image)} {...props} />;
};

export default Image;
